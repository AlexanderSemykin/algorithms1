import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;

import java.awt.Color;
import java.util.ArrayList;

/**
 * 2d-tree implementation
 */
public class KdTree {
    private Node root;
    private int size;

    private static class Node {
        private Point2D p;
        private RectHV rect;

        /**
         * the left/bottom subtree
         */
        private Node lb;

        /**
         * the right/top subtree
         */
        private Node rt;
    }

    public KdTree() {
    }

    public boolean isEmpty() {
        if (root == null) return true;
        else return false;
    }

    public int size() {
        return size;
    }

    public void insert(Point2D p) {
        if (p == null)
            throw new IllegalArgumentException("Argument must not be null!");

        root = insert(root, p, true);
        if (root.rect == null) {
            root.rect = new RectHV(0.0, 0.0, 1.0, 1.0);
        }
    }

    private Node insert(Node node, Point2D p, boolean splitByX) {
        if (node == null) {
            size++;
            node = new Node();
            node.p = p;
            return node;
        }

        if (splitByX) {
            int cmp = Double.compare(p.x(), node.p.x());
            if (cmp < 0) {
                node.lb = insert(node.lb, p, !splitByX);
                if (node.lb.rect == null) {
                    node.lb.rect = new RectHV(node.rect.xmin(), node.rect.ymin(),
                                              node.p.x(), node.rect.ymax());
                }
            }
            else {
                node.rt = insert(node.rt, p, !splitByX);
                if (node.rt.rect == null) {
                    node.rt.rect = new RectHV(node.p.x(), node.rect.ymin(),
                                              node.rect.xmax(), node.rect.ymax());
                }
            }
        }
        else {
            int cmp = Double.compare(p.y(), node.p.y());
            if (cmp < 0) {
                node.lb = insert(node.lb, p, !splitByX);
                if (node.lb.rect == null) {
                    node.lb.rect = new RectHV(node.rect.xmin(), node.rect.ymin(),
                                              node.rect.xmax(), node.p.y());
                }
            }
            else {
                node.rt = insert(node.rt, p, !splitByX);
                if (node.rt.rect == null) {
                    node.rt.rect = new RectHV(node.rect.xmin(), node.p.y(),
                                              node.rect.xmax(), node.rect.ymax());
                }
            }
        }

        return node;
    }

    public boolean contains(Point2D p) {
        if (p == null)
            throw new IllegalArgumentException("Argument must not be null!");

        return contains(root, p, true);
    }

    private boolean contains(Node node, Point2D p, boolean splitByX) {
        if (node == null) return false;

        if (node.p.equals(p)) return true;

        if (splitByX) {
            int cmp = Double.compare(p.x(), node.p.x());
            if (cmp < 0) return contains(node.lb, p, !splitByX);
            else return contains(node.rt, p, !splitByX);
        }
        else {
            int cmp = Double.compare(p.y(), node.p.y());
            if (cmp < 0) return contains(node.lb, p, !splitByX);
            else return contains(node.rt, p, !splitByX);
        }
    }

    public void draw() {
        draw(root, true);
    }

    private void draw(Node node, boolean splitByX) {
        if (node == null) return;

        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(0.01);
        StdDraw.point(node.p.x(), node.p.y());

        if (splitByX) {
            StdDraw.setPenColor(Color.RED);
            StdDraw.setPenRadius();
            StdDraw.line(node.p.x(), node.rect.ymin(), node.p.x(), node.rect.ymax());
        }
        else {
            StdDraw.setPenColor(Color.BLUE);
            StdDraw.setPenRadius();
            StdDraw.line(node.rect.xmin(), node.p.y(), node.rect.xmax(), node.p.y());
        }

        boolean newSplitByX = !splitByX;
        draw(node.lb, newSplitByX);
        draw(node.rt, newSplitByX);
    }

    /**
     * All points that are inside the rectangle (or on the boundary)
     *
     * @param rect
     * @return
     */
    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null)
            throw new IllegalArgumentException("Argument must not be null!");

        ArrayList<Point2D> points = new ArrayList<>();
        range(rect, root, true, points);
        return points;
    }

    private void range(RectHV rect, Node node, boolean splitByX, ArrayList<Point2D> points) {
        if (node == null) return;

        if (rect.contains(node.p))
            points.add(node.p);

        if (splitByX) {
            if (Double.compare(node.p.x(), rect.xmin()) < 0) {
                range(rect, node.rt, !splitByX, points);
            }
            else if (Double.compare(node.p.x(), rect.xmax()) > 0) {
                range(rect, node.lb, !splitByX, points);
            }
            else {
                range(rect, node.lb, !splitByX, points);
                range(rect, node.rt, !splitByX, points);
            }
        }
        else {
            if (Double.compare(node.p.y(), rect.ymin()) < 0) {
                range(rect, node.rt, !splitByX, points);
            }
            else if (Double.compare(node.p.y(), rect.ymax()) > 0) {
                range(rect, node.lb, !splitByX, points);
            }
            else {
                range(rect, node.lb, !splitByX, points);
                range(rect, node.rt, !splitByX, points);
            }
        }
    }


    /**
     * A nearest neighbor in the set to point; null if the set is empty
     *
     * @param p
     * @return
     */
    public Point2D nearest(Point2D p) {
        if (p == null)
            throw new IllegalArgumentException("Argument must not be null!");

        if (isEmpty())
            return null;

        return nearest(root, true, p, root.p);
    }

    private Point2D nearest(Node node, boolean splitByX,
                            Point2D p, Point2D champ) {
        if (node == null) return champ;

        Point2D newChamp = champ(champ, node.p, p);
        if (node.rt != null && node.rt.rect.contains(p)) {
            newChamp = nearest(node.rt, !splitByX, p, newChamp);
            if (needCheckAnotherBranch(node.lb, newChamp, p)) {
                newChamp = nearest(node.lb, !splitByX, p, newChamp);
            }
        }
        else {
            newChamp = nearest(node.lb, !splitByX, p, newChamp);
            if (needCheckAnotherBranch(node.rt, newChamp, p)) {
                newChamp = nearest(node.rt, !splitByX, p, newChamp);
            }
        }

        return newChamp;
    }

    private Point2D champ(Point2D champ, Point2D candidate, Point2D query) {
        double query2ChampDist = query.distanceSquaredTo(champ);
        double query2CandidateDist = query.distanceSquaredTo(candidate);
        int cmp = Double.compare(query2ChampDist, query2CandidateDist);
        if (cmp > 0) return candidate;
        else return champ;
    }

    private boolean needCheckAnotherBranch(Node node, Point2D currChamp, Point2D query) {
        if (node == null)
            return false;
        double query2BranchRectDist = node.rect.distanceSquaredTo(query);
        double champDist = query.distanceSquaredTo(currChamp);
        int cmp = Double.compare(champDist, query2BranchRectDist);
        return cmp > 0;
    }

    public static void main(String[] args) {
    }

}
