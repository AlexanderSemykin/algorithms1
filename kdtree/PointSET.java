import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdOut;

import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Brute-fore implementation.
 * insert(), contains() O(log(n))
 * nearest(), range() O(n)
 */
public class PointSET {
    private TreeSet<Point2D> set;

    public PointSET() {
        set = new TreeSet<>();

    }

    public boolean isEmpty() {
        return set.isEmpty();
    }

    public int size() {
        return set.size();
    }

    public void insert(Point2D p) {
        if (p == null)
            throw new IllegalArgumentException("Argument must not be null!");

        set.add(p);
    }

    public boolean contains(Point2D p) {
        if (p == null)
            throw new IllegalArgumentException("Argument must not be null!");

        return set.contains(p);
    }

    public void draw() {
        for (Point2D p : set)
            p.draw();
    }

    /**
     * All points that are inside the rectangle (or on the boundary)
     *
     * @param rect
     * @return
     */
    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null)
            throw new IllegalArgumentException("Argument must not be null!");

        ArrayList<Point2D> foundPoints = new ArrayList<>();
        for (Point2D p : set) {
            if (rect.contains(p))
                foundPoints.add(p);
        }

        return foundPoints;
    }


    /**
     * A nearest neighbor in the set to point; null if the set is empty
     *
     * @param p
     * @return
     */
    public Point2D nearest(Point2D p) {
        if (p == null)
            throw new IllegalArgumentException("Argument must not be null!");

        if (isEmpty())
            return null;

        double minDistance = Double.MAX_VALUE;
        Point2D nearest = null;
        for (Point2D point : set) {
            double distance = point.distanceSquaredTo(p);
            if (Double.compare(distance, minDistance) < 0) {
                minDistance = distance;
                nearest = point;
            }
        }

        return nearest;
    }

    public static void main(String[] args) {
        PointSET points = new PointSET();
        points.insert(new Point2D(0.1, 0.1));
        points.insert(new Point2D(0.2, 0.2));
        points.insert(new Point2D(0.3, 0.3));
        points.insert(new Point2D(0.4, 0.4));
        points.insert(new Point2D(0.5, 0.5));
        points.insert(new Point2D(0.6, 0.6));
        points.insert(new Point2D(0.7, 0.7));
        points.insert(new Point2D(0.8, 0.8));
        points.insert(new Point2D(0.9, 0.9));
        // points.draw();

        StdOut.println(points.nearest(new Point2D(0.4, 0.6)));
        StdOut.println(points.range(new RectHV(0.2, 0.2, 0.6, 0.6)));
    }
}
