import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {
    private static final byte OPENED_BYTE_VALUE = 1;
    private static final byte FILLED_BYTE_VALUE = 2;

    private final int gridHeight;
    private final int gridSize;
    private final WeightedQuickUnionUF connections;
    private byte[] openedFilled;
    private int openedCount;

    // creates n-by-n grid, with all sites initially blocked
    public Percolation(int n) {
        if (n < 1)
            throw new IllegalArgumentException("n must be >= 1");
        gridHeight = n;
        gridSize = n * n;
        connections = new WeightedQuickUnionUF(gridSize + 1);
        for (int i = 0; i < gridHeight; i++) {
            connections.union(gridSize, i);
        }
        openedFilled = new byte[gridSize];
        openedCount = 0;
    }

    public void open(int row, int col) {
        validateInput(row, col);
        int idx = siteIndex(row, col);
        if (isOpen(idx))
            return;
        open(idx);
        processNeighbors(idx);
        tryToFill(idx);
    }

    // is the site (row, col) open?
    public boolean isOpen(int row, int col) {
        validateInput(row, col);
        return isOpen(siteIndex(row, col));
    }

    // is the site (row, col) full?
    public boolean isFull(int row, int col) {
        validateInput(row, col);
        return isFull(siteIndex(row, col));
    }

    // returns the number of open sites
    public int numberOfOpenSites() {
        return openedCount;
    }

    public boolean percolates() {
        for (int col = 1; col <= gridHeight; col++) {
            if (isFull(gridHeight, col))
                return true;
        }
        return false;
    }

    private void processNeighbors(int siteIndex) {
        int upIdx = siteIndex - gridHeight;
        int downIdx = siteIndex + gridHeight;
        int leftIdx = siteIndex - 1;
        int rightIdx = siteIndex + 1;
        if (upIdx >= 0 && isOpen(upIdx))
            connections.union(siteIndex, upIdx);
        if (downIdx < gridSize && isOpen(downIdx))
            connections.union(siteIndex, downIdx);
        if (siteIndex % gridHeight - 1 >= 0 && isOpen(leftIdx))
            connections.union(siteIndex, leftIdx);
        if (siteIndex % gridHeight + 1 < gridHeight && isOpen(rightIdx))
            connections.union(siteIndex, rightIdx);
    }

    private void validateInput(int row, int col) {
        if (row < 1 || row > gridHeight || col < 1 || col > gridHeight) {
            throw new IllegalArgumentException("Invalid input");
        }
    }

    private int siteIndex(int row, int col) {
        return gridHeight * (row - 1) + (col - 1);
    }

    private boolean isOpen(int i) {
        return openedFilled[i] == OPENED_BYTE_VALUE
                || openedFilled[i] == OPENED_BYTE_VALUE + FILLED_BYTE_VALUE;
    }

    private boolean isFull(int i) {
        return openedFilled[i] == OPENED_BYTE_VALUE + FILLED_BYTE_VALUE
                || tryToFill(i);
    }

    private void open(int i) {
        openedFilled[i] |= OPENED_BYTE_VALUE;
        openedCount++;
    }

    private boolean tryToFill(int i) {
        if (isOpen(i) && connections.find(i) == connections.find(gridSize)) {
            openedFilled[i] |= FILLED_BYTE_VALUE;
            return true;
        }
        return false;
    }

    // test client (optional)
    public static void main(String[] args) {
        Percolation p = new Percolation(6);
        p.open(1, 6);
        p.open(2, 6);
        p.open(3, 6);
        p.open(4, 6);
        p.open(5, 6);
        p.open(5, 5);
        p.open(4, 4);
        p.open(3, 4);
        p.open(2, 4);
        p.open(2, 3);
        p.open(2, 2);
        p.open(2, 1);
        p.open(3, 1);
        p.open(4, 1);
        p.open(5, 1);
        p.open(5, 2);
        p.open(6, 2);
        p.open(5, 4);
        StdOut.print(p.percolates());
    }
}
