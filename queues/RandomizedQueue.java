/* *****************************************************************************
 *  Name:
 *  Date:
 *  Description:
 **************************************************************************** */

import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {

    private Item[] q;
    private int n;


    // construct an empty randomized queue
    public RandomizedQueue() {
        q = (Item[]) new Object[1];
        n = 0;
    }

    // is the randomized queue empty?
    public boolean isEmpty() {
        return n == 0;
    }

    // return the number of items on the randomized queue
    public int size() {
        return n;
    }

    // add the item
    public void enqueue(Item item) {
        validateInput(item);
        if (n == q.length) resize(2 * q.length);
        q[n++] = item;
    }

    // remove and return a random item
    public Item dequeue() {
        throwExceptionOnEmptyData();
        findRandomAndSwapWithLast();
        Item item = q[--n];
        q[n] = null;
        if (n > 0 && n == q.length / 4) resize(q.length / 2);
        return item;
    }

    // return a random item (but do not remove it)
    public Item sample() {
        throwExceptionOnEmptyData();
        return q[StdRandom.uniform(n)];
    }

    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {
        return new RandomIterator();
    }

    private class RandomIterator implements Iterator<Item> {

        private final int[] indexes;
        private int currentIndex;

        public RandomIterator() {
            indexes = StdRandom.permutation(n);
            currentIndex = 0;
        }

        public boolean hasNext() {
            return currentIndex < indexes.length;
        }

        public Item next() {
            if (!hasNext())
                throw new NoSuchElementException("There is no more element");
            int randomIndex = indexes[currentIndex++];
            return q[randomIndex];
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    private void resize(int capacity) {
        Item[] copy = (Item[]) new Object[capacity];
        for (int i = 0; i < n; i++)
            copy[i] = q[i];
        q = copy;
    }

    private void findRandomAndSwapWithLast() {
        int randomIndex = StdRandom.uniform(n);
        Item randomItem = q[randomIndex];
        q[randomIndex] = q[n - 1];
        q[n - 1] = randomItem;
    }

    private void validateInput(Item item) {
        if (item == null)
            throw new IllegalArgumentException("Passing null is prohibited!");
    }

    private void throwExceptionOnEmptyData() {
        if (isEmpty())
            throw new NoSuchElementException("Deque is empty!");
    }

    // unit testing (required)
    public static void main(String[] args) {
        RandomizedQueue<Integer> randomizedQueue = new RandomizedQueue<>();
        randomizedQueue.enqueue(1);
        randomizedQueue.enqueue(2);
        randomizedQueue.enqueue(3);
        randomizedQueue.enqueue(4);
        randomizedQueue.enqueue(5);
        randomizedQueue.dequeue();
        Iterator<Integer> iterator = randomizedQueue.iterator();
        while (iterator.hasNext()) {
            StdOut.println(iterator.next());
        }
        StdOut.println();
        Iterator<Integer> iterator1 = randomizedQueue.iterator();
        while (iterator1.hasNext()) {
            StdOut.println(iterator1.next());
        }

        StdOut.println();
        StdOut.println(randomizedQueue.isEmpty());
        StdOut.println(randomizedQueue.size());
        StdOut.println(randomizedQueue.sample());
    }
}
