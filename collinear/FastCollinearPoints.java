import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

import java.util.Arrays;

public class FastCollinearPoints {
    private static final int COLLINEAR_POINTS_COUNT = 4;

    private LineSegment[] segments;

    public FastCollinearPoints(Point[] points) {
        validate(points);

        segments = new LineSegment[0];
        Double[] segmentsSlopes = new Double[0];
        Point[] segmentsLeftPoints = new Point[0];

        for (int i = 0; i < points.length; i++) {
            Point[] copyPoints = Arrays.copyOf(points, points.length);
            Arrays.sort(copyPoints, points[i].slopeOrder());
            double currSlope = points[i].slopeTo(points[i]);
            double segmentSlope = currSlope;
            int currentPointsCount = 1;
            int maxPointsCount = Integer.MIN_VALUE;
            int currentStartIdx = 0;
            int maxStartIdx = 0;
            for (int j = 0; j < copyPoints.length; j++) {
                if (currSlope == points[i].slopeTo(copyPoints[j])) {
                    currentPointsCount++;
                }
                else {
                    currentPointsCount = 2;
                    currentStartIdx = j;
                    currSlope = points[i].slopeTo(copyPoints[j]);
                }

                if (currentPointsCount > maxPointsCount) {
                    maxPointsCount = currentPointsCount;
                    segmentSlope = currSlope;
                    maxStartIdx = currentStartIdx;
                }
            }

            if (maxPointsCount >= COLLINEAR_POINTS_COUNT) {
                Point[] segmentPoints = new Point[maxPointsCount];
                segmentPoints[0] = points[i];
                for (int n = 0; n < maxPointsCount - 1; n++) {
                    segmentPoints[n + 1] = copyPoints[maxStartIdx + n];
                }
                Arrays.sort(segmentPoints);

                if (Arrays.binarySearch(segmentsLeftPoints, segmentPoints[0]) < 0
                        || Arrays.binarySearch(segmentsSlopes, segmentSlope) < 0) {
                    segmentsLeftPoints = incrementAddAndSort(segmentsLeftPoints, segmentPoints[0],
                                                             true);
                    segmentsSlopes = incrementAddAndSort(segmentsSlopes, segmentSlope, true);
                    segments = incrementAddAndSort(segments,
                                                   new LineSegment(segmentPoints[0], segmentPoints[
                                                           segmentPoints.length - 1]),
                                                   false);
                }

            }
        }
    }

    private <T> T[] incrementAddAndSort(T[] array, T value, boolean sort) {
        array = Arrays.copyOf(array, array.length + 1);
        array[array.length - 1] = value;
        if (sort) {
            Arrays.sort(array);
        }
        return array;
    }

    public int numberOfSegments() {
        return segments.length;
    }

    public LineSegment[] segments() {
        return Arrays.copyOf(segments, segments.length);
    }

    private void validate(Point[] points) {
        if (points == null)
            throw new IllegalArgumentException("Points array is null");

        for (Point p : points)
            if (p == null)
                throw new IllegalArgumentException("There is a null point in the array");

        for (int i = 0; i < points.length; i++)
            for (int j = i + 1; j < points.length; j++)
                if (points[i].compareTo(points[j]) == 0)
                    throw new IllegalArgumentException("Points array contains a repeated point");

    }

    public static void main(String[] args) {
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }

        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        FastCollinearPoints collinear = new FastCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        StdDraw.show();
    }
}
