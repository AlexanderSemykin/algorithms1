import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdOut;

public class Solver {
    private SearchNode goal;
    private final boolean solvable;

    private class SearchNode implements Comparable<SearchNode> {
        private final Board board;
        private final int moves;
        private final SearchNode prevSearchNode;

        public SearchNode(Board board, int moves, SearchNode prevSearchNode) {
            this.board = board;
            this.moves = moves;
            this.prevSearchNode = prevSearchNode;
        }

        public int compareTo(SearchNode other) {
            return Integer.compare(getPriority(), other.getPriority());
        }

        public Board getBoard() {
            return board;
        }

        public int getMoves() {
            return moves;
        }

        public SearchNode getPrevSearchNode() {
            return prevSearchNode;
        }

        public int getPriority() {
            return board.manhattan() + moves;
        }
    }

    // find a solution to the initial board (using the A* algorithm)
    public Solver(Board initial) {
        if (initial == null) {
            throw new IllegalArgumentException("initial board is null");
        }

        solvable = isSolvable(initial);
    }

    public boolean isSolvable() {
        return solvable;
    }

    private boolean isSolvable(Board initial) {
        MinPQ<SearchNode> pq = initPQ(initial);
        MinPQ<SearchNode> tpq = initPQ(initial.twin());

        SearchNode min, tmin;
        do {
            min = handleNode(pq);
            tmin = handleNode(tpq);
        } while (!min.getBoard().isGoal() && !tmin.getBoard().isGoal());

        if (min.getBoard().isGoal()) {
            goal = min;
            return true;
        }
        else return false;
    }

    private MinPQ<SearchNode> initPQ(Board board) {
        MinPQ<SearchNode> pq = new MinPQ<>();
        pq.insert(new SearchNode(board, 0, null));
        return pq;
    }

    private SearchNode handleNode(MinPQ<SearchNode> pq) {
        SearchNode min = pq.delMin();
        Board prevBoard = getPrevBoard(min);
        for (Board board : min.getBoard().neighbors()) {
            if (!board.equals(prevBoard))
                pq.insert(new SearchNode(board, min.getMoves() + 1, min));
        }
        return min;
    }

    private Board getPrevBoard(SearchNode node) {
        return node.getPrevSearchNode() != null
               ? node.getPrevSearchNode().getBoard()
               : null;
    }

    // min number of moves to solve initial board: -1 if unsolvable
    public int moves() {
        if (!isSolvable()) {
            return -1;
        }

        return goal.getMoves();
    }

    // sequence of boards in a shortest solution; null if unsolvable
    public Iterable<Board> solution() {
        if (!isSolvable()) {
            return null;
        }

        SearchNode curr = goal;
        Stack<Board> stack = new Stack<>();
        while (curr != null) {
            stack.push(curr.getBoard());
            curr = curr.getPrevSearchNode();
        }

        return stack;
    }

    public static void main(String[] args) {
        // create initial board from file
        In in = new In(args[0]);
        int n = in.readInt();
        int[][] tiles = new int[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                tiles[i][j] = in.readInt();
        Board initial = new Board(tiles);

        // solve the puzzle
        Solver solver = new Solver(initial);

        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution())
                StdOut.println(board);
        }
    }
}
