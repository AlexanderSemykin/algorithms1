import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;

public class Board {

    private final int n;
    private final int[][] tiles;
    private Board twin;

    // create a board from an n-by-n array of tiles,
    // where tiles[row][col] = tile at (row, col)
    public Board(int[][] tiles) {
        n = tiles[0].length;
        this.tiles = new int[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                this.tiles[i][j] = tiles[i][j];
    }

    // string representation of this board
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(n + System.lineSeparator());
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                s.append(String.format("%2d ", tiles[i][j]));
            }
            s.append(System.lineSeparator());
        }
        return s.toString();
    }

    // board dimension n
    public int dimension() {
        return n;
    }

    // number of tiles out of place
    public int hamming() {
        int hamming = 0;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++) {
                if (tiles[i][j] != 0 && tiles[i][j] != i * n + j % n + 1)
                    hamming++;
            }
        return hamming;
    }

    // sum of Manhattan distances between tiles and goal
    public int manhattan() {
        int manhattan = 0;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++) {
                if (tiles[i][j] != 0) {
                    int goalRowIdx = (tiles[i][j] - 1) / n;
                    int goalColIdx = (tiles[i][j] - 1) % n;
                    int m = Math.abs(i - goalRowIdx) + Math.abs(j - goalColIdx);
                    manhattan += m;
                }
            }
        return manhattan;
    }

    // is this boar the goal board?
    public boolean isGoal() {
        return hamming() == 0;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) return true;
        if (other == null) return false;
        if (other.getClass() != this.getClass()) return false;

        Board otherBoard = (Board) other;
        if (tiles.length != otherBoard.tiles.length) return false;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                if (tiles[i][j] != otherBoard.tiles[i][j])
                    return false;

        return true;
    }


    // all neighboring boards
    public Iterable<Board> neighbors() {
        Stack<Board> neighbors = new Stack<>();
        int rowOf0 = 0, colOf0 = 0;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                if (tiles[i][j] == 0) {
                    rowOf0 = i;
                    colOf0 = j;
                }

        if (rowOf0 - 1 > -1)
            neighbors.push(createNeighbor(rowOf0, colOf0, rowOf0 - 1, colOf0));
        if (colOf0 - 1 > -1)
            neighbors.push(createNeighbor(rowOf0, colOf0, rowOf0, colOf0 - 1));
        if (rowOf0 + 1 < n)
            neighbors.push(createNeighbor(rowOf0, colOf0, rowOf0 + 1, colOf0));
        if (colOf0 + 1 < n)
            neighbors.push(createNeighbor(rowOf0, colOf0, rowOf0, colOf0 + 1));

        return neighbors;
    }


    private Board createNeighbor(int rowOf0, int colOf0, int row, int col) {
        int[][] copy = new int[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                copy[i][j] = tiles[i][j];

        copy[rowOf0][colOf0] = copy[row][col];
        copy[row][col] = 0;

        return new Board(copy);
    }

    // a board that is obtained by exchanging any pair of tales
    public Board twin() {
        if (twin != null)
            return twin;

        int a, b, x, y;
        do {
            a = StdRandom.uniform(0, n);
            b = StdRandom.uniform(0, n);
            x = StdRandom.uniform(0, n);
            y = StdRandom.uniform(0, n);
        } while (tiles[a][b] == 0 || tiles[x][y] == 0
                || (a == x && b == y));

        int[][] tilesCopy = new int[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                tilesCopy[i][j] = tiles[i][j];
        int tmp = tilesCopy[a][b];
        tilesCopy[a][b] = tilesCopy[x][y];
        tilesCopy[x][y] = tmp;

        twin = new Board(tilesCopy);
        return twin;
    }

    public static void main(String[] args) {
        int[][] tiles = { { 8, 1, 3 }, { 4, 0, 2 }, { 7, 6, 5 } };
        Board b = new Board(tiles);
        StdOut.println(b);
        StdOut.println(b.hamming());
        StdOut.println(b.manhattan());
        StdOut.println();

        tiles = new int[][] { { 1, 0, 3 }, { 4, 2, 5 }, { 7, 8, 6 } };
        b = new Board(tiles);
        StdOut.println(b);
        StdOut.println(b.hamming());
        StdOut.println(b.manhattan());
        StdOut.println();

        tiles = new int[][] { { 4, 1, 3 }, { 0, 2, 5 }, { 7, 8, 6 } };
        b = new Board(tiles);
        StdOut.println(b);
        StdOut.println(b.hamming());
        StdOut.println(b.manhattan());

        StdOut.println("neighbors");
        tiles = new int[][] { { 1, 0, 3 }, { 4, 2, 5 }, { 7, 8, 6 } };
        b = new Board(tiles);
        for (Board board : b.neighbors()) {
            StdOut.println(board);
            StdOut.println();
        }

        StdOut.println(b.twin());
        StdOut.println(b.twin());
    }
}
